package pages.elements.utils.filter;


public class FilterUrlParamsBuilder {
    String model = "";
    String mark = "";
    String generation = "";
    String minYear = "";
    String fuelType = "";
    String unSold = "";
    String minMileage = "";


    public FilterUrlParamsBuilder setModel(String model) {
        this.model = model + "/";
        return this;
    }

    public FilterUrlParamsBuilder setMark(String mark) {
        this.mark = mark + "/";
        return this;
    }

    public FilterUrlParamsBuilder setGeneration(String generation) {
        this.generation = generation + "/";
        return this;
    }

    public FilterUrlParamsBuilder setMinYear(String minYear) {
        this.minYear = "minyear=" + minYear + "&";
        return this;
    }

    public FilterUrlParamsBuilder setFuelType(String fuelType) {
        this.fuelType = "fueltype=" + fuelType + "&";
        return this;
    }

    public FilterUrlParamsBuilder setMinMileage(String minMileage) {
        this.minMileage = "minprobeg=" + minMileage + "&";
        return this;
    }

    public FilterUrlParamsBuilder setUnsold(String unsold) {
        this.unSold = "unsold=" + unsold + "&";
        return this;
    }

    private String generateUrlParams() {
        return this.model + this.mark + this.generation;
    }

    private String generateQueryStringParams() {
        return "?" + this.minYear + this.fuelType + this.unSold + this.minMileage;
    }

    public FilterUrlParams build() {
        String urlParams = generateUrlParams();
        String queryStringParams = generateQueryStringParams();
        return new FilterUrlParams(urlParams, queryStringParams);
    }

}
