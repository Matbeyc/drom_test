package pages.elements.utils.filter;

import org.openqa.selenium.WebElement;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mark implements Comparable<Mark> {

    private final String name;
    private final Integer amount;

    public Mark(WebElement markElement) {
        this.name = extractMarkName(markElement);
        this.amount = extractAmountOfSoldMarks(markElement);
    }

    public String getName() {
        return this.name;
    }

    public Integer getAmount() {
        return this.amount;
    }

    private String extractMarkName(WebElement mark) {
        Pattern regex = Pattern.compile("([�-��-�a-zA-Z-]+)");
        Matcher regexMatcher = regex.matcher(mark.getText());
        if (regexMatcher.find()) {
            return regexMatcher.group(1);
        }
        return "";
    }

    private int extractAmountOfSoldMarks(WebElement mark) {
        Pattern regex = Pattern.compile("\\((.*?)\\)");
        Matcher regexMatcher = regex.matcher(mark.getText());
        while (regexMatcher.find()) {
            if (!Objects.equals(regexMatcher.group(1), "0")) {
                return Integer.parseInt(regexMatcher.group(1));
            }
        }
        return 0;
    }

    @Override
    public int compareTo(Mark o) {
        return Integer.compare(this.amount, o.amount);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Mark mark))
            return false;
        return Objects.equals(mark.name, name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + amount;
        return result;
    }
}
