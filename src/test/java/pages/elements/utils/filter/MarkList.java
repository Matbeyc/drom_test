package pages.elements.utils.filter;

import org.openqa.selenium.WebElement;

import java.util.*;


public class MarkList {


    private final List<Mark> markList;


    public MarkList(List<WebElement> markElementList) {
        this.markList = create(markElementList);
    }

    public void display() {
        System.out.format("| %-22s | %-22s | %n", "�����", "���������� ����������");
        for (Mark mark : markList) {
            System.out.format("| %-22s | %-22s | %n", mark.getName(), mark.getAmount());
        }
    }

    public void display(Integer amountOfMarks) {
        sortDesc();
        System.out.format("| %-22s | %-22s | %n", "�����", "���������� ����������");
        for (int i = 0; i < amountOfMarks; i++) {
            System.out.format("| %-22s | %-22s | %n", markList.get(i).getName(), markList.get(i).getAmount());
        }
    }

    public void sortDesc() {

        markList.sort(Collections.reverseOrder());
    }

    public void sort() {
        Collections.sort(markList);
    }


    private List<Mark> create(List<WebElement> allMarkElements) {
        Set<Mark> markSet = new HashSet<>();
        List<Mark> markList = new ArrayList<>();
        for (WebElement markElement : allMarkElements) {
            Mark mark = new Mark(markElement);
            if (!markSet.contains(mark)) {
                markList.add(mark);
            }
            markSet.add(mark);
        }
        return markList;
    }
}
