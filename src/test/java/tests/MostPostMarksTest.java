package tests;
import pages.elements.FilterElement;
import org.junit.jupiter.api.Test;
import pages.MainPage;
import tests.baseTests.BaseMobileTest;

public class MostPostMarksTest extends BaseMobileTest {

    @Test
    public void mostPostMarksTest() {
        MainPage mainPage = new MainPage();
        mainPage.open();
        FilterElement filterElement = mainPage.getFilterElement();
        filterElement.displayTopPostMarks(20);
    }
}
